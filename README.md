Get started
===========

Most stuff here should be more or less self-explanatory and/or documented in
place. However, to get started, you need to do a few things:

1. Please make sure that your user and SSH keys are included in our
   [internal repository](https://chaos.expert/himmel/interna/). Please submit
   a pull request according to the guidelines there and ask another team member
   to run the `deploy-users.yml` playbook to have your account added everywhere.

2. Copy `ansible.cfg.sample` to `ansible.cfg` and change your username so that
   Ansible will be able to login as you on the managed machines.

3. Clone the internal repository into the `files/` directory so that Ansible
   will be able to access all the user-related data:

        git clone git@chaos.expert:himmel/interna.git files/
